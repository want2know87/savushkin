// Аудио
var isPlaying = true

function toggleAudio() {
  if (isPlaying) {
    document.getElementById('audio').volume = 1
  } else {
    document.getElementById('audio').volume = 0
  }
}
(function changeSound() {
  if (window.matchMedia("(max-width: 768px)").matches) {
    document.getElementById('audio').pause();
    $('#audioIsOn').removeClass('active')
    $('#audioIsOff').addClass('active')
    $('#music-button').on('click', function () {
      document.getElementById('audio').play();
      if ($('#audioIsOff').hasClass('active')) {
        isPlaying = true
        toggleAudio()
        $('#audioIsOff').removeClass('active')
        $('#audioIsOn').addClass('active')
      } else {
        isPlaying = false
        toggleAudio()
        $('#audioIsOff').addClass('active')
        $('#audioIsOn').removeClass('active')
      }
    });

  } else {
    document.getElementById('audio').play();
    $('#music-button').on('click', function () {
      if ($('#audioIsOn').hasClass('active')) {
        $('#audioIsOn').removeClass('active')
        $('#audioIsOff').addClass('active')
      } else {
        $('#audioIsOff').removeClass('active')
        $('#audioIsOn').addClass('active')
      }
      isPlaying = !isPlaying
      toggleAudio()
    });
  }
}())



// Видео
// $('#video').get(0).play()
$('#presentationTwo, .language, .snowman').hide()
$('#video').on('ended', function() {
  $('#presentationOne').hide()
  $('#presentationTwo, .language, .snowman').fadeIn()
})

$('#video').on('playing', function() {
  $('.play-video').css({
    visibility: 'hidden',
    opacity: '0',
    display: 'none'
  })
})

function addControls() {
  if (window.matchMedia("(max-width: 768px)").matches) {
    // document.getElementById('video').setAttribute('controls', true);
    document.getElementById('video').controls = true;
  } else {
    //document.getElementById('video').setAttribute('controls', false)
    document.getElementById('video').controls = false;
  }
}
addControls()

$(window).on('resize', addControls)


$('#video').on('close', function () {
  $('#presentationOne, .play-video').hide()
  $('#presentationTwo, .language, .snowman').fadeIn()
})

$('.play-video').fadeIn()

// Скипнуть видео
function skipVideo() {
  $('#presentationOne').hide()
  $('#presentationTwo, .language, .snowman').fadeIn()
  window.localStorage.removeItem('skipVideo');

  return false
}

//Включить видео
$('.play-video').on('click', function() {
  $('#video')[0].play()
})

//Поделиться
var Share = {
  vkontakte: function (e, t, n, o) {
    url = "https://vk.com/share.php?", url += "url=" + encodeURIComponent(e), url += "&title=" + encodeURIComponent(t), url += "&description=" + encodeURIComponent(o), url += "&image=" + encodeURIComponent(n), url += "&noparse=true", Share.popup(url)
  },
  odnoklassniki: function (e, t) {
    url = "http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1", url += "&st.comments=" + encodeURIComponent(t), url += "&st._surl=" + encodeURIComponent(e), Share.popup(url)
  },
  facebook: function (e, t, n, o) {
    url = "http://www.facebook.com/sharer.php?s=100", url += "&p[title]=" + encodeURIComponent(t), url += "&p[summary]=" + encodeURIComponent(o), url += "&p[url]=" + encodeURIComponent(e), url += "&p[images][0]=" + encodeURIComponent(n), Share.popup(url)
  },
  twitter: function (e, t) {
    url = "http://twitter.com/share?", url += "text=" + encodeURIComponent(t), url += "&url=" + encodeURIComponent(e), url += "&counturl=" + encodeURIComponent(e), Share.popup(url)
  },
  mailru: function (e, t, n, o) {
    url = "http://connect.mail.ru/share?", url += "url=" + encodeURIComponent(e), url += "&title=" + encodeURIComponent(t), url += "&description=" + encodeURIComponent(o), url += "&imageurl=" + encodeURIComponent(n), Share.popup(url)
  },
  popup: function (e) {
    window.open(e, "", "toolbar=0,status=0,width=626,height=436")
  }
}


//Language switch

$('.language a').on('click', function() {
  $('.language a').removeClass('active')
  $(this).addClass('active')
  window.localStorage.setItem('skipVideo', 1);
})


//Magnific popup
$('.actions--greeting').magnificPopup({
  type: 'inline',

  fixedContentPos: false,
  fixedBgPos: true,

  overflowY: 'auto',

  closeBtnInside: false,
  preloader: false,

  midClick: true,
  removalDelay: 300,
  mainClass: 'my-mfp-zoom-in'
});

//Form submit
$('#form-dialog').submit(function (e) {
  e.preventDefault();
  var target = e.target;
  var email, lang, url, gresponse;
  email = $('[name="email"]').val();
  lang = $('[name="lang"]').val();
  url = $('[name="url"]').val();
  gresponse = $('[name="g-recaptcha-response"]').val();
  $.post(
    $(target).attr('action'),
    {
      email: email,
      gresponse: gresponse,
      url: url,
      lang: lang
    },
    function () {
      $.magnificPopup.open({
        items: {
          src: '#submit-result',
        },
        type: 'inline',
        showCloseBtn: false,
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: false,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in'
      }, 0);
      setTimeout(function () {
        $.magnificPopup.close();
      }, 3000);
    }
  );
});

if (window.localStorage.getItem('skipVideo')) {
  skipVideo();
}
$('[name="email"]').tagsInput({
  defaultText: $('[name="email"]').attr('placeholder'),
  onChange: function(){
      var input = $(this).siblings('.tagsinput');
      var maxLen = 5; // e.g.
      if(input.children('span.tag').length >= maxLen){
          input.children('div').hide();
      }
      else{
          input.children('div').show();
      }
  }
});
