<?php

require '_translate.php';
$config = require('_config.php');

$uri    = $_SERVER['REQUEST_URI'];
$scheme = isset($_SERVER['HTTPS']) ? "https" : "http";
$host   = $_SERVER['HTTP_HOST'];
$lang   = isset($_GET['lang']) ? $_GET['lang'] : 'ru';
$ruClass = $lang === 'ru' ? 'language--ru active' : 'language--ru';
$enClass = $lang === 'en' ? 'language--en active' : 'language--en';


if(strpos($uri, 'index.php') !== false) {
    $location = $lang !== 'ru' ? "Location: /$lang" : "Location: /";
    header($location, true, 301);
    exit();
}

$url = "$scheme://{$host}{$uri}";
$img = str_replace('/en', '', rtrim($url, '/'))."/img/Shering_{$lang}.png";
$vk = [t('share_title', $lang), t('share_descr', $lang)];
$facebook = [t('share_title', $lang), t('share_descr', $lang)];
$odnoklassniki = [t('share_title', $lang)];
$twitter = [t('share_title', $lang)];
$shares = [
  'vk' => [
    'icon' => 'fa fa-vk',
    'onclick' => "Share.vkontakte('$url', '$vk[0]', '$img', '$vk[1]'); return false;",
  ],
  'facebook' => [
    'icon' => 'fa fa-facebook',
    'onclick' => "Share.facebook('$url', '$facebook[0]', '$img', '$facebook[1]'); return false;",
  ],
  'odnoklassniki' => [
    'icon' => 'fa fa-odnoklassniki',
    'onclick' => "Share.odnoklassniki('$url', '$odnoklassniki[0]'); return false;",
  ],
  'twitter' => [
    'icon' => 'fa fa-twitter',
    'onclick' => "Share.twitter('$url', '$twitter[0]'); return false;",
  ],
];
$video = $lang === 'en' ? 'Greeting_Card_Main_EN' : 'Greeting_Card_Main_RU';
?>
<!DOCTYPE html>
<html lang="<?=$lang?>">
  <head>
    <meta charset="utf-8">
    <title><?=t('title', $lang)?></title>
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="theme-color" content="#000">
    <meta name="msapplication-navbutton-color" content="#000">
    <meta name="apple-mobile-web-app-status-bar-style" content="#000">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
    <style>body { opacity: 0; overflow-x: hidden; } html { background-color: #fff; }</style>
    <!--[if lt IE 9]>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <![endif]-->
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <meta property="og:title" content="<?=t('share_title', $lang)?>"/>
    <meta property="og:description" content="<?=t('share_descr', $lang)?>"/>
    <meta property="og:image" content="<?=$img?>"/>
    <meta property="og:url" content="<?=$url?>"/>
  </head>
  <body>
    <span class="audio-icon" id="music-button">
      <svg class="audio-icon--item active" xmlns="http://www.w3.org/2000/svg" width="39.469" height="30.5" viewbox="0 0 39.469 30.5" id="audioIsOn">
        <path d="M64,31a15.231,15.231,0,0,0-12.878,7.073H45.29A4.29,4.29,0,0,0,41,42.363v7.773a4.29,4.29,0,0,0,4.29,4.29h5.828A15.231,15.231,0,0,0,64,61.5a1.094,1.094,0,0,0,1.1-1.1V32.1A1.1,1.1,0,0,0,64,31ZM50.638,52.228H45.29A2.091,2.091,0,0,1,43.2,50.136V42.363a2.091,2.091,0,0,1,2.092-2.092h5.348V52.228ZM62.9,59.26a13.119,13.119,0,0,1-10.061-6.235V39.481A13.119,13.119,0,0,1,62.9,33.247V59.26ZM69.4,37.74a1.1,1.1,0,1,0-1.254,1.807,8.18,8.18,0,0,1,0,13.43,1.107,1.107,0,0,0-.277,1.53,1.1,1.1,0,0,0,1.53.277A10.381,10.381,0,0,0,69.4,37.74Zm4.249-4.55A1.1,1.1,0,1,0,72.4,35a13.708,13.708,0,0,1,0,22.514,1.107,1.107,0,0,0-.277,1.53,1.1,1.1,0,0,0,1.53.277A15.909,15.909,0,0,0,73.651,33.19Z" transform="translate(-41 -31)"></path>
      </svg>
      <svg class="audio-icon--item" xmlns="http://www.w3.org/2000/svg" width="41.719" height="30.5" viewbox="0 0 41.719 30.5" id="audioIsOff">
        <path d="M152.436,40.411L146.847,46l5.576,5.589a0.879,0.879,0,0,1,0,1.247,0.85,0.85,0,0,1-.62.261,0.885,0.885,0,0,1-.62-0.261l-5.59-5.589L140,52.836a0.858,0.858,0,0,1-.62.261,0.885,0.885,0,0,1-.62-1.508L144.353,46l-5.589-5.589a0.882,0.882,0,1,1,1.247-1.247l5.589,5.589,5.589-5.589A0.882,0.882,0,0,1,152.436,40.411ZM134,61.5a15.232,15.232,0,0,1-12.878-7.073H115.29a4.29,4.29,0,0,1-4.29-4.29V42.363a4.29,4.29,0,0,1,4.29-4.289h5.828A15.232,15.232,0,0,1,134,31a1.1,1.1,0,0,1,1.1,1.1V60.4A1.094,1.094,0,0,1,134,61.5ZM120.638,40.271H115.29a2.091,2.091,0,0,0-2.092,2.092v7.773a2.091,2.091,0,0,0,2.092,2.092h5.348V40.271ZM132.9,33.247a13.118,13.118,0,0,0-10.061,6.235V53.025A13.118,13.118,0,0,0,132.9,59.26V33.247Z" transform="translate(-111 -31)"></path>
      </svg>
    </span>
    <div class="language">
      <a class="<?=$ruClass?>" href="/2017/">Ru</a>
      <a class="<?=$enClass?>" href="/2017/en">En</a>
    </div>
    <img class="snowman" src="img/snowman.png" alt="Савушкин">
    <img class="play-video" src="img/tap.png" alt="play video" onclick="$(this).hide();">
    <div class="presentation-one" id="presentationOne">
      <audio id="audio" autoplay="true" loop="true">
        <source src="audio/last_christmas.mp3" type="audio/mpeg">
        <source src="audio/last_christmas.aac" type="audio/aac">
        <source src="audio/last_christmas.wav" type="audio/wav">
        <source src="audio/last_christmas.ogg" type="audio/ogg">
      </audio>
      <div class="video">
        <video class="video--item" id="video" autoplay muted>
          <source src="video/<?=$video?>.webm" type='video/webm; codecs="vp8.0, vorbis"'>
          <source src="video/<?=$video?>.mp4" type='video/mp4; codecs="avc1.4D401E, mp4a.40.2"'>
          <source src="video/<?=$video?>.ogv" type='video/ogg; codecs="theora, vorbis"'>
        </video><!--  Safari autoplay fix -->
        <script>
          document.getElementById('video').play();
          //- $("#video").get(0).play()
        </script>
      </div>
    </div>
    <div class="presentation-two" id="presentationTwo" style="background-image: url(img/bg.jpg);">
      <div class="logo">
        <div class="logo--balls"><img class="logo--balls--img" src="img/balls.png" alt="Савушкин"></div><a class="logo--link" href="javascript:void(0)"><img class="logo--img" src="img/logo_<?=$lang?>.png" alt="Савушкин"></a>
      </div>
      <div class="greeting">
        <h1 class="greeting--title"><?=t('greeting_title', $lang)?></h1>
        <h2 class="greeting--subtitle"><?=t('greeting_subtitle', $lang)?></h2>
      </div>
      <div class="actions">
        <div class="actions--socials">
          <?php foreach ($shares as $share): ?>
          <a class="actions--socials--item"
             href="javascript:void(0)"
             onclick="<?=$share['onclick']?>"
          >
             <i class="<?=$share['icon']?>"></i>
          </a>
          <?php endforeach; ?>
        </div>
        <span class="actions--or"><?=t('actions_or', $lang)?></span>
        <a class="actions--greeting" href="#form-dialog">
          <?=t('actions_greeting', $lang)?>
          <img class="actions--greeting--bird" src="img/bird.png" alt="Савушкин">
        </a>
        <form class="zoom-anim-dialog mfp-hide form-dialog" action="send.php" method="post" id="form-dialog">
          <h3 class="form-dialog--title"><?=t('form_dialog_title', $lang)?></h3>
          <input type="hidden" name="lang" value="<?=$lang?>">
          <input type="hidden" name="url" value="<?=$url?>">
          <input class="form-dialog--input" type="text" name="email" placeholder="<?=t('email_placeholder', $lang)?>">
          <div class="g-recaptcha" data-sitekey="<?=$config['captcha_key']?>"></div>
          <button class="form-dialog--submit" type="submit" data="<?=t('submit_placeholder', $lang)?>">
            <?=t('submit_button', $lang)?>
          </button>
        </form>
        <div class="zoom-anim-dialog mfp-hide form-dialog" style="text-align: center;" id="submit-result">
          <h3 class="form-dialog--title"><?=t('submit_result', $lang)?></h3>
        </div>
      </div>
    </div>
    <link rel="stylesheet" href="css/main.css?v=12">
    <link rel="stylesheet" href="libs/jquery.tagsinput/jquery.tagsinput.min.css"></link>
    <script src="libs/jquery/jquery-3.2.1.min.js"></script>
    <script src="libs/magnific/jquery.magnific-popup.min.js"></script>
    <script src="libs/jquery.tagsinput/jquery.tagsinput.min.js"></script>
    <script src="js/common.js?v=12"></script>
  </body>
</html>
