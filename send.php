<?php

require '_translate.php';
$config = require('_config.php');

$lang      = isset($_POST['lang'])      ? $_POST['lang']      : 'ru';
$url       = isset($_POST['url'])       ? $_POST['url']       : '';
$emails    = isset($_POST['email'])     ? $_POST['email']     : '';
$gresponse = isset($_POST['gresponse']) ? $_POST['gresponse'] : '';
$prefix = str_replace('/en', '', rtrim($url, '/'));
$img = $prefix."/img/mail/savushkin-ny-{$lang}.png";
$img1 = $prefix."/img/mail/savushkin-ny1-{$lang}.png";
$img2 = $prefix."/img/mail/savushkin-ny2-{$lang}.png";
$img3 = $prefix."/img/mail/savushkin-ny3-{$lang}.png";
?>
<?php ob_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" style="background:#ffffff!important;margin:0 auto;max-width:1024px;min-width:320px">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Savushkin</title>
    <style>
        @media only screen and (max-width:749px) {
            body {
                max-width: 320px!important;
                height: 100%!important;
                min-height: 309px!important;
            }
            table tr td img {
                max-width: 320px!important;
            }
            table tr td a img {
                max-width: 320px!important;
            }
        }
    </style>
</head>
<body style="-moz-box-sizing:border-box;-webkit-box-sizing:border-box;Margin:0;background:#ffffff!important;box-sizing:border-box;margin:0 auto;max-width:750px;height:724px;min-width:320px;padding:0;text-align:center;width:100%!important">
    <table style="border-collapse:collapse;margin-left:auto;margin-right:auto">
      <tr>
        <td style="padding:0">
          <img src="<?=$img1?>" alt="link" />
        </td>
      </tr>
      <tr>
        <td style="padding:0">
          <a href="<?=$url?>" target="_blank" style="display: block">
             <img src="<?=$img2?>" alt="link" />
          </a>
        </td>
      </tr>
      <tr>
        <td style="padding:0">
          <img src="<?=$img3?>" alt="link" />
        </td>
      </tr>
    </table>
    <!-- prevent Gmail on iOS font size manipulation -->
    <div style="display:none;white-space:nowrap;font:15px courier;line-height:0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
</body>
</html>
<?php $content = ob_get_clean(); ?>
<?php
if (!empty($lang) && !empty($url) && !empty($emails) && !empty($gresponse)) {
    $emails = explode(',', $emails);
    $send = !$config['debug'];
    if (!$config['debug']) {
        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=utf-8';
        $headers[] = 'From: '.$config['info_email'];
        foreach ($emails as $email) {
            $send = $send && mail($email, 'Савушкин поздравляет с Новым годом и Рождеством', $content, join("\r\n", $headers));
        }
    }
    $send = $send ? 'yes' : 'no';
    $text = '';
    foreach ($emails as $email) {
        $text .= "$send : $email : $url: $gresponse\n";
    }
    file_put_contents(__DIR__ . '/result.txt', $text, FILE_APPEND);
}
