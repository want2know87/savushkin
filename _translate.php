<?php

$messages = [
    'title' => [
        'ru' => 'Главная',
        'en' => 'Home',
    ],
    'greeting_title' => [
        'ru' => 'С Рождеством и Новым годом!',
        'en' => 'Merry Christmas and Happy New Year!',
    ],
    'greeting_subtitle' => [
        'ru' => 'Всегда ваш, «Савушкин продукт»',
        'en' => 'Always yours «Savushkin product»',
    ],
    'actions_greeting' => [
        'ru' => 'Поздравить!',
        'en' => 'Congratulate!',
    ],
    'form_dialog_title' => [
        'ru' => 'Кого поздравляем?',
        'en' => 'Your congratulations to',
    ],
    'email_placeholder' => [
        'ru' => 'Эл. почта',
        'en' => 'Email',
    ],
    'submit_button' => [
        'ru' => 'Поздравить!',
        'en' => 'Congratulate!',
    ],
    'submit_result' => [
        'ru' => 'Ваше поздравление отправлено!',
        'en' => 'Your congratulation was sent!',
    ],
    'actions_or' => [
        'ru' => 'или',
        'en' => 'or',
    ],
    'share_title' => [
        'ru' => 'С Рождеством и Новым годом!',
        'en' => '«Merry Christmas and Happy New Year!',
    ],
    'share_descr' => [
        'ru' => 'С Рождеством и Новым годом! Всегда Ваш «Савушкин продукт»',
        'en' => '«Merry Christmas and Happy New Year! «Always yours «Savushkin product»',
    ],
];

function t($key, $lang = 'ru') {
    global $messages;
    return $messages[$key][$lang];
}
